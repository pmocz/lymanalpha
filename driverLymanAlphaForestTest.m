% Philip Mocz (2015) Lyman Alpha Flux
clc;   clear all;   close all;   rng(42)
% addpath('/mnt/odyssey/ArepoRuns/analysis/PMAnalysisPackage/');

% simulation snapshot to read
datapath =  '/mnt/ghernquist/Output/Arepo/movingCT_Tests/2015/CosmoBox25/Powell256fullphys10/';
filename = [datapath, 'snapshot_030.hdf5']; % z=2.73


%% Units (we will convert code units to cgs)
unitM = h5readatt(filename,'/Header','UnitMass_in_g');
unitL = h5readatt(filename,'/Header','UnitLength_in_cm');
unitV = h5readatt(filename,'/Header','UnitVelocity_in_cm_per_s');
unitT = unitL/unitV;
convert_to_gauss = sqrt(unitM/unitL)/(unitT);
convert_to_cm = unitL;
convert_to_g_per_cm3 = unitM/unitL^3;
convert_to_erg = unitM*unitL^2/unitT^2;
convert_to_cm_per_s = unitV;
convert_to_1_per_s = 1/unitT;
cm_to_Mpc = 3.24078e-25;
M_H = 1.6737236e-24; % Mass of hydrogen (g)
kB = 1.380658e-16; % Boltzmann constant (erg/Kelvin)
c = 2.99792458e10; % Speed of light (cm/s)
sigmaT = 6.652458558e-25; % Thompson cross section (cm^2)
lambda0 = 1215.6e-8; % LyAlpha wavelength (cm)
f_oscillator =  0.41615; % LyAlpha oscillator strength
sigma_a = sqrt(3*pi*sigmaT/8)*f_oscillator*lambda0; % (cm^2)


%% Load data
boxsize = h5readatt(filename,'/Header','BoxSize') * convert_to_cm; % cm/h
redshift = h5readatt(filename,'/Header','Redshift');
atime = h5readatt(filename,'/Header','Time'); % a = 1/(1+z)
Omega0 = h5readatt(filename,'/Header','Omega0');
OmegaLambda = h5readatt(filename,'/Header','OmegaLambda');
OmegaBaryon = h5readatt(filename,'/Header','OmegaBaryon');
h100 = h5readatt(filename,'/Header','HubbleParam'); % ~0.68  (set h = 1 to keep scaling with h in the analysis)
Hz = 100*h100 * sqrt(1+Omega0*(1/atime-1)+OmegaLambda*(atime^2-1))/atime; % km/s/Mpc
Hz = Hz * 3.24077929e-20; % 1/s
convert_distance_to_vel = atime/h100 * Hz;
convert_distance_to_proper = atime/h100;

datasetname = '/PartType0/Coordinates';
pos = double(h5read(filename,datasetname)) * convert_to_cm; % cm/h
x = pos(1,:)';
y = pos(2,:)';
z = pos(3,:)';
datasetname = '/PartType0/Density';
density = double(h5read(filename,datasetname)) * convert_to_g_per_cm3;
datasetname = '/PartType0/Volume';
vol = double(h5read(filename,datasetname)) * convert_to_cm^3;
datasetname = '/PartType0/MagneticField';
Bfield = double(h5read(filename,datasetname)) * convert_to_gauss;
Bx = Bfield(1,:)';
By = Bfield(2,:)';
Bz = Bfield(3,:)';
Bnorm = sqrt(Bx.^2+By.^2+Bz.^2);
datasetname = '/PartType0/InternalEnergy';
u = double(h5read(filename,datasetname)) * convert_to_cm_per_s^2;
datasetname = '/PartType0/StarFormationRate';
sfr = double(h5read(filename,datasetname));
datasetname = '/PartType0/NeutralHydrogenAbundance';
A_neutralH = double(h5read(filename,datasetname));
datasetname = '/PartType0/ElectronAbundance';
N_e = double(h5read(filename,datasetname));
%X_H = 0.76; % hydroden fraction by mass
%mu = 1./(X_H.*(0.75+N_e)+0.25);
%mu = (4 / (8 - 5 * (1 - X_H))); % assuming full ionization
datasetname = '/PartType0/GFM_Metals';
X_H = double(h5read(filename,datasetname));
X_H = X_H(1,:)';
datasetname = '/PartType0/GFM_Metallicity';
metallicity = double(h5read(filename,datasetname));
yhelium = (1 - X_H - metallicity) ./ (4. * X_H);
mu = (1 + 4 * yhelium) ./ (1 + yhelium + N_e);
gamma = 5/3; % ideal gas adiabatic index
T = u * (gamma-1) .* mu * M_H / kB; % gas temperature in Kelvin
datasetname = '/PartType0/Velocities';
vel = double(h5read(filename,datasetname)) * convert_to_cm_per_s * sqrt(atime); %peculiar velocities, important that it is multiplied by the square root of the expansion factor; % physical
vx = vel(1,:)';
vy = vel(2,:)';
vz = vel(3,:)';

%select only neutral hydrogen (treats properly the star forming gas and
%eliminates molecular hydrogen) - effect is small
A_neutralH = getNeutralHydrogen(u/convert_to_cm_per_s^2, density/convert_to_g_per_cm3/atime^3, sfr, h100, A_neutralH);



%% Get random line-of-sight (LOS) of length boxsize
Nbins = 512; % LOS is sampled at Nbins points
% seed = 42;
% [densityLOS, TLOS, vpeculiarLOS, HILOS] = getRandomLOS( x, y, z, A_neutralH.*density, T, vx, vy, vz, A_neutralH, boxsize, Nbins, seed );
[densityLOS, TLOS, vpeculiarLOS, HILOS] = getCenterLOS( x, y, z, A_neutralH.*density, T, vx, vy, vz, A_neutralH, boxsize, Nbins );

%% Calculate LyA spectra
tau_H1 = getSpectrum( h100*h100*densityLOS/atime^3, TLOS, vpeculiarLOS, Nbins, boxsize, convert_distance_to_vel, convert_distance_to_proper, Hz, kB, sigma_a, c, M_H );


%% Plot LOS
fig_handle = figure(1);
set(fig_handle,'Position', [20, 20, 800, 800]);
dd = linspace(0,boxsize,Nbins)*cm_to_Mpc; % Mpc
subplot(4,1,1)
plot(dd,ones(Nbins,1),'k');
axis([0 boxsize*cm_to_Mpc 0 1.1])
ylabel('$\exp(-\tau)$','interpreter','latex','fontsize',16)
hold on
plot(dd,exp(-tau_H1),'b','linewidth',2);
subplot(4,1,2)
plot(dd,log10(HILOS),'b','linewidth',2);
axis([0 boxsize*cm_to_Mpc -6 -4])
ylabel('$\log_{10}({\rm HI})$','interpreter','latex','fontsize',16)
subplot(4,1,3)
plot(dd,log10(TLOS),'b','linewidth',2);
axis([0 boxsize*cm_to_Mpc 3.5 5])
ylabel('$\log_{10}(T)$','interpreter','latex','fontsize',16)
subplot(4,1,4)
plot(dd,vpeculiarLOS/10^5,'b','linewidth',2);
axis([0 boxsize*cm_to_Mpc -300 300])
ylabel('$v_p~({\rm km}~{\rm s}^{-1})$','interpreter','latex','fontsize',16)
xlabel('$d_{\rm comoving}~(h^{-1}{\rm Mpc})$','interpreter','latex','fontsize',16)
hold off

print('/home/pmocz/Desktop/foo.eps','-depsc')
