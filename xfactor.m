function x = xfactor(u, rho, rhothresh, tempclouds, tempsupernova, factorEVP)

PROTONMASS = 1.67262178e-24;
GAMMA_MINUS1 = 5./3. - 1;
HYDROGEN_MASSFRAC = 0.76;
BOLTZMANN = 1.38065e-16;

meanweight = 4 / (1 + 3 * HYDROGEN_MASSFRAC); % note: assuming NEUTRAL GAS
ucold = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * tempclouds;
ucold = ucold * 1.0e-10;

meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));  % note: assuming FULL ionization

uhot = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * tempsupernova;
uhot = uhot * 1.0e-10;
factorEVP = factorEVP * (rho / rhothresh).^-0.8;
uSN = uhot ./ (1 + factorEVP) + ucold;

x = (uSN - u) ./ (uSN - ucold);

end

