function fHI = getNeutralHydrogen(u, rho, sfr, h, A_neutral)
% get rid of molecules (Leroy et al. 2008)


GAMMA_MINUS1 = 5./3. - 1;
BOLTZMANN = 1.38065e-16;
MSUN = 1.989e33;
MPC = 3.085678e24;

fHI = A_neutral;
P = GAMMA_MINUS1 * rho .* u * h * h / BOLTZMANN * (1e20 * MSUN / MPC^3.);
index = sfr > 0;
rhothresh = 879328.;
tempclouds = 1000;
tempsupernova = 5.73e7;
factorEVP = 573;
fHI_out = xfactor( u(index), rho(index), rhothresh, tempclouds, tempsupernova, factorEVP );
fHI(index) = fHI_out;
P0 = 10^4.23;
alpha = 0.8;
fHI = fHI .* HIfrac(fHI .* P, P0, alpha);

end



% Equation (12) of Blitz & Rosolowsky
function fHI = HIfrac(P, P0, alpha)
x = (P / P0).^alpha;
fHI = 1. ./ (1. + x);
end