function [densityLOS, TLOS, vpeculiarLOS, HILOS] = getRandomLOS( x, y, z, rho, T, vx, vy, vz, A, boxsize, Nbins, seed )
%GETRANDOMLOS get a random line-of-sight thru the box
%   interpolate gas particle values onto points along the LOS
%   LOS has length boxsize
rng(seed);
% choose start and end positions for LOS
x1 = boxsize*(rand*0.8+0.1);
y1 = boxsize*(rand*0.8+0.1);
z1 = boxsize*(rand*0.8+0.1);
randDir = randi(3,1); % choose LOS along x, y, or z
switch randDir
    case 1 % x
        x1 = 0;
        x2 = boxsize;
        y2 = y1;
        z2 = z1;
    case 2 % y
        y1 = 0;
        x2 = x1;
        y2 = boxsize;
        z2 = z1;
    case 3 % z
        z1 = 0;
        x2 = x1;
        y2 = y1;
        z2 = boxsize;
end

xx = linspace(x1,x2,Nbins);
yy = linspace(y1,y2,Nbins);
zz = linspace(z1,z2,Nbins);
ss = [xx(:) yy(:) zz(:)];

% Keep only simulation points near the LOS
expand = 0.01;
switch randDir
    case 1 % x
        cut = (y1-y).^2+(z1-z).^2 < (expand*boxsize)^2;
    case 2 % y
        cut = (x1-x).^2+(z1-z).^2 < (expand*boxsize)^2;
    case 3 % z
        cut = (x1-x).^2+(y1-y).^2 < (expand*boxsize)^2;
end
x = x(cut);
y = y(cut);
z = z(cut);
rho = rho(cut);
T = T(cut);
vx = vx(cut);
vy = vy(cut);
vz = vz(cut);
A = A(cut);

% Pad simulation points with periodic copies of points, due to periodic
% boundary conditions
switch randDir
    case 1 % x
        padR = x<boxsize*expand;
        padL = x>boxsize*(1-expand);
        x = [x; x(padL)-boxsize; x(padR)+boxsize];
        y = [y; y(padL); y(padR)];
        z = [z; z(padL); z(padR)];
    case 2 % y
        padR = y<boxsize*expand;
        padL = y>boxsize*(1-expand);
        x = [x; x(padL); x(padR)];
        y = [y; y(padL)-boxsize; y(padR)+boxsize];
        z = [z; z(padL); z(padR)];
    case 3 % z
        padR = z<boxsize*expand;
        padL = z>boxsize*(1-expand);
        x = [x; x(padL); x(padR)];
        y = [y; y(padL); y(padR)];
        z = [z; z(padL)-boxsize; z(padR)+boxsize];
end
rho = [rho; rho(padL); rho(padR)];
T = [T; T(padL); T(padR)];
vx = [vx; vx(padL); vx(padR)];
vy = [vy; vy(padL); vy(padR)];
vz = [vz; vz(padL); vz(padR)];
A = [A; A(padL); A(padR)];

% interpolate onto the LOS
warning('off','MATLAB:nearlySingularMatrix') % ignore near singular matrix errors in interpolation, which is fine
%tic;
%densityLOS = griddatan([x y z],rho,ss,'linear');
%TLOS = griddatan([x y z],T,ss,'linear');
switch randDir
    case 1 % x
        %vpeculiarLOS = 0; %griddatan([x y z],vx,ss,'linear');
        allFields = [rho T vx A];
    case 2 % y
        %vpeculiarLOS = 0; %griddatan([x y z],vy,ss,'linear');
        allFields = [rho T vy A];
    case 3 % z
        %vpeculiarLOS = 0; %griddatan([x y z],vz,ss,'linear');
        allFields = [rho T vz A];
end
%HILOS = griddatan([x y z],A,ss,'linear');
allLOS = mygriddatanlinear([x y z],allFields,ss);
densityLOS = allLOS(:,1);
TLOS = allLOS(:,2);
vpeculiarLOS = allLOS(:,3);
HILOS = allLOS(:,4);
warning('on','MATLAB:nearlySingularMatrix')
%toc;
end

