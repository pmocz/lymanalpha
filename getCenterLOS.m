function [densityLOS, TLOS, vpeculiarLOS, HILOS] = getCenterLOS( x, y, z, rho, T, vx, vy, vz, A, boxsize, Nbins )
%GETCENTERLOS get line-of-sight thru center of box
%   interpolate gas particle values onto points along the LOS
%   LOS has length boxsize

% choose start and end positions for LOS
x1 = boxsize*0.0;
y1 = boxsize*0.5;
z1 = boxsize*0.5;
x2 = boxsize*1.0;
y2 = y1;
z2 = z1;

xx = linspace(x1,x2,Nbins);
yy = linspace(y1,y2,Nbins);
zz = linspace(z1,z2,Nbins);
ss = [xx(:) yy(:) zz(:)];

% Keep only simulation points near the LOS
expand = 0.01;

cut = (y1-y).^2+(z1-z).^2 < (expand*boxsize)^2;

x = x(cut);
y = y(cut);
z = z(cut);
rho = rho(cut);
T = T(cut);
vx = vx(cut);
vy = vy(cut);
vz = vz(cut);
A = A(cut);

% Pad simulation points with periodic copies of points, due to periodic
% boundary conditions

padR = x < boxsize*expand;
padL = x > boxsize*(1-expand);
x = [x; x(padL)-boxsize; x(padR)+boxsize];
y = [y; y(padL); y(padR)];
z = [z; z(padL); z(padR)];

rho = [rho; rho(padL); rho(padR)];
T = [T; T(padL); T(padR)];
vx = [vx; vx(padL); vx(padR)];

A = [A; A(padL); A(padR)];

% interpolate onto the LOS
warning('off','MATLAB:nearlySingularMatrix') % ignore near singular matrix errors in interpolation, which is fine
allFields = [rho T vx A];

allLOS = mygriddatanlinear([x y z], allFields, ss);

densityLOS = allLOS(:,1);
TLOS = allLOS(:,2);
vpeculiarLOS = allLOS(:,3);
HILOS = allLOS(:,4);

warning('on','MATLAB:nearlySingularMatrix')

%densityLOS = griddatan([x y z],rho,ss,'linear');
%TLOS = griddatan([x y z],T,ss,'linear');
%vpeculiarLOS = griddatan([x y z],vx,ss,'linear');
%HILOS = griddatan([x y z],A,ss,'linear');

end

