function tau_H1 = getSpectrum( densityLOS, TLOS, vpeculiarLOS, Nbins, boxsize, convert_distance_to_vel, convert_distance_to_proper, Hz, kB, sigma_a, c, M_H )
%GETSPECTRUM calculate Lyman Alpha spectrum
% follows Appendix A4 of
% http://arxiv.org/pdf/astro-ph/9805119v1.pdf
% convolution

tau_H1 = zeros(Nbins,1);
vmax = boxsize * convert_distance_to_vel; % cm/s
dzgrid = boxsize * convert_distance_to_proper / Nbins; % cm
dvbin = dzgrid * Hz; % velocity bin size (cm/s)
% spectrum convolution
for i = 1:Nbins
    for j = 1:Nbins
        u_H1  = dvbin*(j-1);
        u_H1 = u_H1 + vpeculiarLOS(j);
        vdiff_H1 = abs(dvbin*(i-1) - u_H1);
        if vdiff_H1 > vmax/2      
            vdiff_H1 = vmax - vdiff_H1; % due to periodic boundary conditions
        end
        b_H1 = sqrt(2*kB*TLOS(j)/M_H); % thermal broadening (cm/s)
        columndensity = dzgrid * densityLOS(j) / M_H;
        profile_H1 = exp(-(vdiff_H1/b_H1)^2);  % assume H1 has a thermal broadened Gaussian profile
        single_line = sigma_a*c/b_H1 / sqrt(pi) * columndensity * profile_H1;
        tau_H1(j) = tau_H1(j) + single_line;
    end
end

end

