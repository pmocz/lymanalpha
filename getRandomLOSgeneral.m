function [densityLOS, TLOS, vpeculiarLOS, HILOS] = getRandomLOS( x, y, z, rho, T, vx, vy, vz, A, boxsize, Nbins )
%GETRANDOMLOS get a random line-of-sight thru the box
%   interpolate gas particle values onto points along the LOS
%   LOS has length boxsize

x1 = boxsize*rand;
y1 = boxsize*rand;
z1 = boxsize*rand;

x = mod(x-x1,boxsize);
y = mod(y-y1,boxsize);
z = mod(z-z1,boxsize);
x1 = 0.05*boxsize;
y1 = 0.05*boxsize;
z1 = 0.05*boxsize;

randomTheta = acos(.9) + (pi/2-acos(.9)) *rand;
randomZ = 0.05 + 0.9*rand;
randomX = sqrt(1-randomZ^2)*cos(randomTheta);
randomY = sqrt(1-randomZ^2)*sin(randomTheta);

fprintf(['getting LOS: ' num2str(randomX) ', ' num2str(randomY) ', '  num2str(randomZ) '\n']);

x2 = x1 + boxsize*randomX;
y2 = y1 + boxsize*randomY;
z2 = z1 + boxsize*randomZ;

xx = linspace(x1,x2,Nbins);
yy = linspace(y1,y2,Nbins);
zz = linspace(z1,z2,Nbins);
ss = [xx(:) yy(:) zz(:)];

% distance to LOS squared
cut = (((x1-x).^2+(y1-y).^2+(z1-z).^2) .* ((x2-x1).^2+(y2-y1).^2+(z2-z1).^2) ...
    - ((x1-x).*(x2-x1)+(y1-y).*(y2-y1)+(z1-z).*(z2-z1)).^2 ) ./ ...
    (((x2-x1).^2+(y2-y1).^2+(z2-z1).^2)) < (0.02*boxsize)^2;

warning('off','MATLAB:nearlySingularMatrix')
densityLOS = griddatan([x(cut) y(cut) z(cut)],rho(cut),ss,'linear');
TLOS = griddatan([x(cut) y(cut) z(cut)],T(cut),ss,'linear');
vpeculiarLOS = randomX*griddatan([x(cut) y(cut) z(cut)],vx(cut),ss,'linear') + randomY*griddatan([x(cut) y(cut) z(cut)],vy(cut),ss,'linear') + randomZ*griddatan([x(cut) y(cut) z(cut)],vz(cut),ss,'linear');
HILOS = griddatan([x(cut) y(cut) z(cut)],A(cut),ss,'linear');
warning('on','MATLAB:nearlySingularMatrix')

end

