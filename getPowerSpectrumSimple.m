function [ pxx_avg, k, pxxc_avg ] = getPowerSpectrumSimple( tau_H1, scale, tau_eff, Nbins, NLOS, boxsize, cm_to_Mpc )
%GETPOWERSPECTRUM calculate power spectrum density
%   Detailed explanation goes here

tau_H1_scaled = tau_H1 * scale;
flux_H1_local = exp(-tau_H1_scaled);%/exp(-tau_eff) - 1;

pxx_avg = 0;
pxxc_avg = 0;
k = (1:(Nbins/2+1))'*2*pi/boxsize/cm_to_Mpc;
k = k(2:end);

% get the power-spectrum density
for i = 1:NLOS
    
    Fk = fft(flux_H1_local(:,i));
    % Want power P(k)= F(k).re*F(k).re+F(k).im*F(k).im
    % http://www.cmp.caltech.edu/~mcc/Chaos_Course/Lesson6/Power.pdf
    % https://holometer.fnal.gov/GH_FFT.pdf
    power = zeros(Nbins/2+1,1);
    power(1) = abs(Fk(1)).^2;
    power(2:Nbins/2) = abs(Fk(2:Nbins/2)).^2 + abs(Fk(Nbins:-1:Nbins/2+2)).^2;
    power(Nbins/2+1) = abs(Fk(Nbins/2+1)).^2;
    power = power / Nbins^2;
    
    % chop off large scale power due to periodic box
    power = power(2:end);
    
    % smooth the power
    windowSize = 5;
    tophat = ones(1,windowSize) / windowSize;
    power_smoothed = filter(tophat,1,power);
    
    pxx_avg = pxx_avg + power_smoothed/NLOS;
end

pxx_avg = pxx_avg ./ k;


end

