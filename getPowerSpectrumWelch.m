function [ pxx_avg, k, pxxc_avg ] = getPowerSpectrumWelch( tau_H1, scale, tau_eff, Nbins, NLOS, boxsize, cm_to_Mpc )
%GETPOWERSPECTRUM calculate power spectrum density
%   Detailed explanation goes here

tau_H1_scaled = tau_H1 * scale;
flux_H1_local = exp(-tau_H1_scaled);%/exp(-tau_eff) - 1;

pxx_avg = 0;
pxxc_avg = 0;

% get the power-spectrum density
for i = 1:NLOS
    %[pxx, k, pxxc] = pwelch(flux_H1_local(:,NLOS),hamming(Nbins/2),Nbins/4,Nbins/2,(Nbins/2+1)*2*pi/boxsize/cm_to_Mpc,'ConfidenceLevel',0.95);
    [pxx, k, pxxc] = pwelch(flux_H1_local(:,NLOS),[],[],[],(Nbins/2+1)*2*pi/boxsize/cm_to_Mpc,'ConfidenceLevel',0.95);
    
    % chop off large scale power due to periodic box
    k = k(4:end);
    pxx = pxx(4:end);
    pxxc = pxxc(4:end);
    
    % smooth the power
    windowSize = 3;
    tophat = ones(1,windowSize) / windowSize;
    pxx_smoothed = filter(tophat,1,pxx);
    pxxc_smoothed = filter(tophat,1,pxxc);
    
    pxx_avg = pxx_avg + pxx_smoothed/NLOS;
    pxxc_avg = pxxc_avg + pxxc_smoothed/NLOS;
end

end

