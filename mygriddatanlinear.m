function yi = mygriddatanlinear(x,y,xi)
%GRIDDATAN Data gridding and hyper-surface fitting (dimension >= 2).
%   GRID MULTIPLE Y AT ONCE - FASTER THAN GRIDDATA!!
%   YI = GRIDDATAN(X,Y,XI) fits a hyper-surface of the form Y = F(X) to the
%   data in the (usually) nonuniformly-spaced vectors (X, Y).  GRIDDATAN
%   interpolates this hyper-surface at the points specified by XI to
%   produce YI. XI can be nonuniform.
%
%   X is of dimension m-by-n, representing m points in n-D space. Y is of
%   dimension m-by-1, representing m values of the hyper-surface F(X). XI
%   is a vector of size p-by-n, representing p points in the n-D space
%   whose surface value is to be fitted. YI is a vector of length p
%   approximating the values F(XI).  The hyper-surface always goes through
%   the data points (X,Y).  XI is usually a uniform grid (as produced by
%   MESHGRID).


%LINEAR Triangle-based linear interpolation

%   Reference: David F. Watson, "Contouring: A guide
%   to the analysis and display of spacial data", Pergamon, 1994.

% Triangularize the data
tri = delaunayn(x);


% Find the nearest triangle (t)
[t,p] = tsearchn(x,tri,xi);
K = ~isnan(t);

m1 = size(xi,1);
nInterp = size(y,2);
yi = zeros(m1,nInterp);

for i = 1:nInterp
  yi(K,:) = yi(K,:) + bsxfun(@times,p(K,i),y(tri(t(K),i),:));
end
yi(~K,:) = NaN;

% for i = 1:m1
%   if ~isnan(t(i))
%      yi(i) = p(i,:)*y(tri(t(i),:));
%   end
% end

